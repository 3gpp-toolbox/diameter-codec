import os
import typing
import unittest
from datetime import datetime
from ipaddress import ip_address
from urllib.parse import urlparse

from codec.diameter.diameter import DiameterHeader, Diameter, Avp
from codec.diameter.dictionary import DictionaryLayout, DefaultDictionaryLayout


class MyTestCase(unittest.TestCase):

    def setUp(self):
        xml_dict_path: str = f'{os.getcwd()}/src/unittest/python/dictionary/Diameter.xml'
        dictionary_layout: DictionaryLayout = DefaultDictionaryLayout(xml_dict_path)
        self.diameter: Diameter = Diameter(dictionary_layout)

    def test_message_001(self):
        avp_set: typing.Tuple = (
            Avp("Framed-IP-Address", '192.168.0.1'),
            Avp("Event-Timestamp", int(datetime.now().timestamp())),
            Avp("Acct-Session-Id", "test-29091980"),
            Avp("Origin-State-Id", 1),
            Avp("Origin-Host", "dmtsrv001.zte.com.cn"),
            Avp("Origin-Realm", "zte.com.cn"),
            Avp("Redirect-Host", "aaa://host.example.com;transport=tcp"),
            Avp("Redirect-Host-Usage", "ALL_USER"),
            Avp("3GPP-User-Location-Info", '8200f210303900f2100123a30a000000'),
            Avp("WLAN-Identifier",
                (
                    Avp("SSID", "etisalat-roam001"),
                )),
            Avp("EPS-Subscribed-QoS-Profile", (
                Avp("Allocation-Retention-Priority", (
                    Avp("Priority-Level", 1),
                    Avp("QoS-Class-Identifier", "QCI_5"),
                )),
            )),
        )

        header: DiameterHeader = DiameterHeader(application_id=16777250, command_code=265, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)
        self.assertEqual(header.application_id, decoded_header.application_id)
        self.assertEqual(header.command_code, decoded_header.command_code)
        self.assertEqual(header.version, decoded_header.version)
        self.assertEqual(header.end_to_end_identifier, decoded_header.end_to_end_identifier)
        self.assertEqual(header.hop_by_hop_identifier, decoded_header.hop_by_hop_identifier)
        self.assertEqual(header.command_flag_err, decoded_header.command_flag_err)
        self.assertEqual(header.command_flag_proxy, decoded_header.command_flag_proxy)
        self.assertEqual(header.command_flag_req, decoded_header.command_flag_req)
        self.assertEqual(header.command_flag_ret, decoded_header.command_flag_ret)
        self.assertEqual(header.avp_set, decoded_header.avp_set)
        self.assertGreater(decoded_header.length, 0)

    def test_message_002(self):
        avp_set_request: typing.Tuple = (
            Avp("Framed-IP-Address", ip_address('192.168.0.1')),
            Avp("Host-IP-Address", ip_address('fde4:2c6e:55c4:105:a00:27ff:fe0b:7859')),
            Avp("Host-IP-Address", '192.168.0.1'),
            Avp("Event-Timestamp", int(datetime.now().timestamp())),
            Avp("Acct-Session-Id", "test-29091980"),
            Avp("Origin-State-Id", 1),
            Avp("Origin-Host", "dmtsrv001.zte.com.cn"),
            Avp("Origin-Realm", "zte.com.cn"),
            Avp("Redirect-Host", urlparse("aaa://host.example.com;transport=tcp")),
            Avp("Redirect-Host-Usage", "ALL_USER"),
            Avp("WLAN-Identifier",
                (
                    Avp("SSID", "etisalat-roam001"),
                )),
            Avp("EPS-Subscribed-QoS-Profile", (
                Avp("Allocation-Retention-Priority", (
                    Avp("Priority-Level", 1),
                    Avp("QoS-Class-Identifier", "QCI_5"),
                )),
            )),
        )

        avp_set_expected: typing.Tuple = (
            Avp("Framed-IP-Address", '192.168.0.1'),
            Avp("Host-IP-Address", 'fde4:2c6e:55c4:105:a00:27ff:fe0b:7859'),
            Avp("Host-IP-Address", '192.168.0.1'),
            Avp("Event-Timestamp", int(datetime.now().timestamp())),
            Avp("Acct-Session-Id", "test-29091980"),
            Avp("Origin-State-Id", 1),
            Avp("Origin-Host", "dmtsrv001.zte.com.cn"),
            Avp("Origin-Realm", "zte.com.cn"),
            Avp("Redirect-Host", "aaa://host.example.com;transport=tcp"),
            Avp("Redirect-Host-Usage", "ALL_USER"),
            Avp("WLAN-Identifier",
                (
                    Avp("SSID", "etisalat-roam001"),
                )),
            Avp("EPS-Subscribed-QoS-Profile", (
                Avp("Allocation-Retention-Priority", (
                    Avp("Priority-Level", 1),
                    Avp("QoS-Class-Identifier", "QCI_5"),
                )),
            )),
        )

        header: DiameterHeader = DiameterHeader(application_id=16777250, command_code=265, avp_set=avp_set_request)
        encoded_message: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_message)
        self.assertEqual(header.application_id, decoded_header.application_id)
        self.assertEqual(header.command_code, decoded_header.command_code)
        self.assertEqual(header.version, decoded_header.version)
        self.assertEqual(header.end_to_end_identifier, decoded_header.end_to_end_identifier)
        self.assertEqual(header.hop_by_hop_identifier, decoded_header.hop_by_hop_identifier)
        self.assertEqual(header.command_flag_err, decoded_header.command_flag_err)
        self.assertEqual(header.command_flag_proxy, decoded_header.command_flag_proxy)
        self.assertEqual(header.command_flag_req, decoded_header.command_flag_req)
        self.assertEqual(header.command_flag_ret, decoded_header.command_flag_ret)
        self.assertEqual(avp_set_expected, decoded_header.avp_set)
        self.assertGreater(decoded_header.length, 0)

    def test_message_003(self):
        avp_set_request: typing.Tuple = (
            Avp("Session-Id", "2aed048b06d21aaff155d1454ac6d8de"),
            Avp("Auth-Session-State", "NO_STATE_MAINTAINED"),
            Avp("Origin-Host", "test-u0001-1-1.diamagent.mme.org"),
            Avp("Destination-Host", "enea.diamagent.org"),
            Avp("Origin-Realm", "mme.org"),
            Avp("Destination-Realm", "diamagent.org"),
            Avp("User-Name", "21910000000011"),
            Avp("Auth-Application-Id", 16777251),
            #   Avp("Visited-PLMN-Id", "310410"),
            Avp("Requested-EUTRAN-Authentication-Info", (
                Avp("Number-Of-Requested-Vectors", 1),
            )),
            Avp("Origin-State-Id", 33818),
            Avp("Front-End-ID", "A-diamrouter_s6a_diamagent-small-1-udm-diamagent-64dcb4fb64-xk8xv"),
        )

        avp_set_expected: typing.Tuple = (
            Avp("Session-Id", "2aed048b06d21aaff155d1454ac6d8de"),
            Avp("Auth-Session-State", "NO_STATE_MAINTAINED"),
            Avp("Origin-Host", "test-u0001-1-1.diamagent.mme.org"),
            Avp("Destination-Host", "enea.diamagent.org"),
            Avp("Origin-Realm", "mme.org"),
            Avp("Destination-Realm", "diamagent.org"),
            Avp("User-Name", "21910000000011"),
            Avp("Auth-Application-Id", 16777251),
            #   Avp("Visited-PLMN-Id", "310410"),
            Avp("Requested-EUTRAN-Authentication-Info", (
                Avp("Number-Of-Requested-Vectors", 1),
            )),
            Avp("Origin-State-Id", 33818),
            Avp("Front-End-ID", "A-diamrouter_s6a_diamagent-small-1-udm-diamagent-64dcb4fb64-xk8xv"),
        )

        header: DiameterHeader = DiameterHeader(application_id=16777251, command_code=318, avp_set=avp_set_request)
        encoded_message: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_message)
        self.assertEqual(header.application_id, decoded_header.application_id)
        self.assertEqual(header.command_code, decoded_header.command_code)
        self.assertEqual(header.version, decoded_header.version)
        self.assertEqual(header.end_to_end_identifier, decoded_header.end_to_end_identifier)
        self.assertEqual(header.hop_by_hop_identifier, decoded_header.hop_by_hop_identifier)
        self.assertEqual(header.command_flag_err, decoded_header.command_flag_err)
        self.assertEqual(header.command_flag_proxy, decoded_header.command_flag_proxy)
        self.assertEqual(header.command_flag_req, decoded_header.command_flag_req)
        self.assertEqual(header.command_flag_ret, decoded_header.command_flag_ret)
        self.assertEqual(avp_set_expected, decoded_header.avp_set)
        self.assertGreater(decoded_header.length, 0)

    # def test_message_004(self):
    #    encoded_message: bytes = b'\x01\x00\x00\xe4\x00\x00\x01>\x01\x00\x00#2\xbb\xdb\xb0\x1f_\tf\x00\x00\x01\x07' \
    #                             b'@\x00\x00(' \
    #                             b'2aed048b06d21aaff155d1454ac6d8de\x00\x00\x01\x08@\x00\x00\x1aenea.diamagent.org' \
    #                             b'\x00\x00\x00\x00\x01(' \
    #                             b'@\x00\x00\x15diamagent.org\x00\x00\x00\x00\x00\x01\x15@\x00\x00\x0c\x00\x00\x00' \
    #                             b'\x01\x00\x00\x01)@\x00\x00 \x00\x00\x01\n@\x00\x00\x0c\x00\x00(' \
    #                             b'\xaf\x00\x00\x01*@\x00\x00\x0c\x00\x00\x13\x8d\x00\x00\x01\x17@\x00\x00$\x00\x00' \
    #                             b'\x05\x80\xc0\x00\x00\x0c\x00\x00(\xaf\x00\x00\x05\x7f\xc0\x00\x00\x0e\x00\x00(' \
    #                             b'\xaf00\x00\x00\x00\x00\x01\x02@\x00\x00\x0c\x01\x00\x00#\x00\x00\x03\xe7\x80\x00' \
    #                             b'\x00\x18\x00\x00)4\x00\x00\x00\x03\x00\x00\x03a\x00\x00\x00\x01'
    #
    #    decoded_header: DiameterHeader = self.diameter.decode(encoded_message)
    #   pprint(decoded_header)


if __name__ == '__main__':
    unittest.main()
