import os
import typing
import unittest

from codec.diameter.diameter import DiameterHeader, Diameter, Avp
from codec.diameter.dictionary import DictionaryLayout, DefaultDictionaryLayout


class MyTestCase(unittest.TestCase):

    def setUp(self):
        xml_dict_path: str = f'{os.getcwd()}/src/unittest/python/dictionary/Diameter.xml'
        dictionary_layout: DictionaryLayout = DefaultDictionaryLayout(xml_dict_path)
        self.diameter: Diameter = Diameter(dictionary_layout)

    def test_message_001(self):
        avp_set: typing.Tuple = (
            Avp("Result-Code", "DIAMETER_SUCCESS"),
            Avp("Result-Code", "DIAMETER_COMMAND_UNSUPPORTED"),
        )

        header: DiameterHeader = DiameterHeader(application_id=0, command_code=257, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)
        self.assertEqual(header.avp_set, decoded_header.avp_set)

    def test_message_002(self):
        avp_set: typing.Tuple = (
            Avp("Result-Code", "DIAMETER_COMMAND_UNSUPPORTED"),
            Avp("Result-Code", "DIAMETER_REALM_NOT_SERVED"),
            Avp("Result-Code", "ELECTION_LOST"),

            Avp("Result-Code", 3001),  # DIAMETER_COMMAND_UNSUPPORTED
            Avp("Result-Code", 3003),  # DIAMETER_REALM_NOT_SERVED
            Avp("Result-Code", 4003),  # ELECTION_LOST
        )

        header: DiameterHeader = DiameterHeader(application_id=16777251, command_code=257, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)
        avp_set_expected = (
            Avp(name='Result-Code', data='DIAMETER_COMMAND_UNSUPPORTED'),
            Avp(name='Result-Code', data='DIAMETER_REALM_NOT_SERVED'),
            Avp(name='Result-Code', data='ELECTION_LOST'),
            Avp(name='Result-Code', data='DIAMETER_COMMAND_UNSUPPORTED'),
            Avp(name='Result-Code', data='DIAMETER_REALM_NOT_SERVED'),
            Avp(name='Result-Code', data='ELECTION_LOST')
        )

        self.assertEqual(avp_set_expected, decoded_header.avp_set)

    def test_message_003(self):
        avp_set: typing.Tuple = (
            Avp("Result-Code", "DIAMETER_COMMAND_UNSUPPORTED"),
            Avp("Experimental-Result-Code", "DIAMETER_ERROR_USER_UNKNOWN"),
            Avp("Experimental-Result-Code", 5452),
        )

        header: DiameterHeader = DiameterHeader(application_id=16777252, command_code=257, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)

        avp_set_expected = (
            Avp("Result-Code", "DIAMETER_COMMAND_UNSUPPORTED"),
            Avp("Experimental-Result-Code", "DIAMETER_ERROR_USER_UNKNOWN"),
            Avp("Experimental-Result-Code", "DIAMETER_ERROR_RAT_TYPE_NOT_ALLOWED"),
        )

        self.assertEqual(avp_set_expected, decoded_header.avp_set)

    def test_message_004(self):
        avp_set: typing.Tuple = (
            Avp("NAS-Port-Type", "Async"),
            Avp("NAS-Port-Type", "Sync"),
            Avp("NAS-Port-Type", "ISDN"),
            Avp("NAS-Port-Type", "ISDN-V120"),
            Avp("NAS-Port-Type", "Cable"),
            Avp("NAS-Port-Type", "xDSL"),
        )

        header: DiameterHeader = DiameterHeader(application_id=16777219, command_code=257, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)
        self.assertEqual(header.avp_set, decoded_header.avp_set)


if __name__ == '__main__':
    unittest.main()
