import os
import typing
import unittest

from codec.diameter.diameter import DiameterHeader, Diameter, Avp
from codec.diameter.dictionary import DictionaryLayout, DefaultDictionaryLayout


class MyTestCase(unittest.TestCase):

    def setUp(self):
        xml_dict_path: str = f'{os.getcwd()}/src/unittest/python/dictionary/Diameter.xml'
        dictionary_layout: DictionaryLayout = DefaultDictionaryLayout(xml_dict_path)
        self.diameter: Diameter = Diameter(dictionary_layout)

    def test_message_001(self):
        avp_set: typing.Tuple = (
            Avp("Product-Name", "hello"),
            Avp("Origin-Realm", "zte.com.cn"),
            Avp("Origin-Host", "dmtsrv001.zte.com.cn"),
            Avp("Host-IP-Address", "192.168.0.1"),
            Avp("Vendor-Id", 10415),
            Avp("Product-Name", "dummy-product"),
            Avp("Inband-Security-Id", "TLS"),
            Avp("Vendor-Specific-Application-Id", (
                Avp("Vendor-Id", 10415),
                Avp("Acct-Application-Id", 1),
                Avp("Auth-Application-Id", 1),
            )),
            Avp("Vendor-Specific-Application-Id", (
                Avp("Auth-Application-Id", 2),
                Avp("Acct-Application-Id", 2),
                Avp("Vendor-Id", 10415),
            )),
        )

        header: DiameterHeader = DiameterHeader(application_id=0, command_code=257, avp_set=avp_set)
        encoded_header: bytes = self.diameter.encode(header)
        decoded_header: DiameterHeader = self.diameter.decode(encoded_header)
        self.assertEqual(header.avp_set, decoded_header.avp_set)

    def test_message_002(self):
        avp_set: typing.Tuple = (
            Avp("Inband-Security-Id", 1000),  # unknown AVP code
        )

        header: DiameterHeader = DiameterHeader(application_id=0, command_code=257, avp_set=avp_set)
        with self.assertRaises(ValueError):
            self.diameter.encode(header)


if __name__ == '__main__':
    unittest.main()
