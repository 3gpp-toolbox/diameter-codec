#   -*- coding: utf-8 -*-
# https://buildmedia.readthedocs.org/media/pdf/pybuilder/latest/pybuilder.pdf
# https://dev.to/awwsmm/managing-your-python-project-with-git-and-pybuilder-21if
# https://github.com/simonski/buildnumber/blob/main/USER_GUIDE.md
import os
from pathlib import Path

from pybuilder.core import use_plugin, init, Author

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.install_dependencies")
use_plugin("python.flake8")
use_plugin("python.coverage")
use_plugin("python.distutils")
use_plugin('python.pycharm')

# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------

name = "diameter-codec"
version = os.popen('buildnumber get').read().strip()
authors = [Author("Kresimir Popovic", "kresimir.popovic@gmail.com")]
license = "European Union Public Licence 1.2 (EUPL 1.2)"
default_task = "publish"
url = "https://gitlab.com/3gpp-toolbox/diameter-codec"
distutils_classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Developers",
    f"License :: OSI Approved :: {license}",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11"
]

requires_python = ">=3.6"
distutils_readme_file_type = "text/markdown"
description = (Path(__file__).parent / "README.md").read_text(encoding="utf8")
distutils_setup_keywords = ["diameter", "codec", "rfc6733"]


@init
def initialize_prod_env(project):
    project.build_depends_on_requirements("requirements.txt")  # dev mode: dependencies not packed
    project.set_property('coverage_break_build', False)
    project.set_property('distutils_readme_file_type', distutils_readme_file_type)
    project.set_property('distutils_setup_keywords', distutils_setup_keywords)
    project.set_property('distutils_classifiers', distutils_classifiers)
